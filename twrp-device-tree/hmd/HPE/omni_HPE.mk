#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from HPE device
$(call inherit-product, device/hmd/HPE/device.mk)

PRODUCT_DEVICE := HPE
PRODUCT_NAME := omni_HPE
PRODUCT_BRAND := Nokia
PRODUCT_MODEL := Nokia C21 Plus
PRODUCT_MANUFACTURER := hmd

PRODUCT_GMS_CLIENTID_BASE := android-hmd

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="T19661AA1_Natv-user 11 RP1A.201005.001 16038 release-keys"

BUILD_FINGERPRINT := Nokia/Hope_00WW/HPE:11/RP1A.201005.001/00WW_1_270:user/release-keys
