#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Include GSI keys
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

# A/B
$(call inherit-product, $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)

PRODUCT_PACKAGES += \
    android.hardware.boot@1.2-impl \
    android.hardware.boot@1.2-impl.recovery \
    android.hardware.boot@1.2-service

PRODUCT_PACKAGES += \
    update_engine \
    update_engine_sideload \
    update_verifier

PRODUCT_PACKAGES += \
    checkpoint_gc \
    otapreopt_script

# fastbootd
PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.1-impl-mock \
    fastbootd

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Partitions
PRODUCT_BUILD_SUPER_PARTITION := false
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \
    para.sh \
    loading.sh \
    log_to_csv.sh \
    total.sh \

PRODUCT_PACKAGES += \
    fstab.enableswap \
    init.s9863a3h10.usb.rc \
    init.s9863a1c10.usb.rc \
    init.s9863a1h10_22nm_go_32b.rc \
    init.factorytest.rc \
    init.s9863a1h10.usb.rc \
    init.storage.rc \
    init.T19661AA1.rc \
    init.s9863a3c10.usb.rc \
    init.s9863a1h20.usb.rc \
    init.s9863a1h10_go.rc \
    init.ram.rc \
    init.s9863a1h10.rc \
    init.s9863a1h10_go_32b.rc \
    init.s9863a1h10_go.usb.rc \
    init.s9863a1c10.rc \
    init.s9863a2h10.usb.rc \
    init.cali.rc \
    init.s9863a1h10_go_32b.usb.rc \
    init.s9863a1h20.rc \
    init.s9863a1h10_22nm_go_32b.usb.rc \
    init.s9863a3c10.rc \
    init.s9863a2h10.rc \
    init.T19661AA1.usb.rc \
    init.s9863a3h10.rc \
    ueventd.s9863a1c10.rc \
    init.recovery.s9863a1h10_22nm_go_32b.rc \
    init.recovery.s9863a3h10.rc \
    ueventd.s9863a1h10_22nm_go_32b.rc \
    ueventd.T19661AA1.rc \
    init.recovery.s9863a1c10.rc \
    ueventd.s9863a2h10.rc \
    init.recovery.s9863a1h10_go.rc \
    init.recovery.s9863a1h10.rc \
    ueventd.s9863a1h10.rc \
    init.recovery.s9863a1h10_go_32b.rc \
    init.recovery.s9863a1h20.rc \
    ueventd.s9863a1h20.rc \
    init.recovery.s9863a2h10.rc \
    ueventd.s9863a3h10.rc \
    ueventd.s9863a1h10_go.rc \
    init.recovery.s9863a3c10.rc \
    init.recovery.T19661AA1.rc \
    ueventd.s9863a3c10.rc \
    ueventd.s9863a1h10_go_32b.rc \
    init.recovery.common.rc \

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rootdir/etc/fstab.enableswap:$(TARGET_VENDOR_RAMDISK_OUT)/first_stage_ramdisk/fstab.enableswap

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 30

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/hmd/HPE/HPE-vendor.mk)
